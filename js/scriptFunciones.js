//------------- FUNCION 1 PRACTICA 4-------------
/*Diseña una función que reciba como argumento un arreglo de valores
enteros de 20 posiciones , regrese el valor promedio de los elementos del arreglo.*/
var arregloNumeros = [9,8,4,9,5,5,2,5,8,8,2,5,7,2,1,5,1,2,3,6];

function promedio(resultado){
    
    var suma = 0;
    

    for(var i=0;i<resultado.length;i++){
        suma += resultado[i]; 
        
    }
    
    return suma / arregloNumeros.length; 
    
}

function ImprimirParrafoDePromedio(){
    let parrafoPromedio = document.getElementById('parrafoPromedio');
    parrafoPromedio.innerHTML = parrafoPromedio.innerHTML + "Los datos ingresados son: " + arregloNumeros + "<br/>" + "El resultado del promedio es: " + promedio(arregloNumeros) + "<br/><br/>" ;
    
}

console.log("El resultado es: " + promedio(arregloNumeros));

//--------------------------------------------------------------------------


//-------------- FUNCION 2 PRACTICA 4-----------------------
/*Diseñe una función que reciba como argumento un arreglo de 20 valores
numéricos enteros, y me regrese la cantidad de valores pares que existe en el */

var arregloPares = [67,51,26,73,64,19,6,92,53,79,22,4,47,24,48,35,13,44,59,59];

function pares(numPares){
    var contador = 0;

    for(var i=0;i<numPares.length;i++){
        
        if(numPares[i] % 2 == 0){
            contador = contador + 1; 
        }

        
    }

    return contador;

}

function ImprimirParrafoPares(){
    let parrafoPares = document.getElementById('parrafoPares');
    parrafoPares.innerHTML = parrafoPares.innerHTML + "Los datos ingresados son: " + arregloPares + "<br/>" + "El resultado es: " + pares(arregloPares) + " números pares" + "<br/><br/>" ;
}

console.log("El total de numeros pares es: " + pares(arregloPares))


//------------------------------------------------------------------------
//-------------- FUNCION 3 PRACTICA 4-----------------------
/*Diseñe una función que reciba como argumento un arreglo de 20 valores
numéricos enteros, ordene los valores del arreglo de mayor a menor.*/



function ordenar(ordenador){
    for(i=0;i<ordenador.length;i++){
        ordenador.sort((a,b) => b-a)
    }

    return ordenador
}

function ImprimirParrafoOrdenado(){
    var arregloOrdenar = [34,50,71,20,75,95,77,65,38,31,27,44,29,69,46,49,6,11,57,54]
    let parrafoOrdenado = document.getElementById('parrafoOrdenado');
    parrafoOrdenado.innerHTML = parrafoOrdenado.innerHTML + "Los datos ingresados son: " + arregloOrdenar + "<br/>" + "El arreglo ordenado de Mayor a Menor es: " + ordenar(arregloOrdenar) + "<br/><br/>" 
}


// Función para limpiar
function limpiador(){
    parrafoPromedio.innerHTML = "";
    parrafoPares.innerHTML = "";
    parrafoOrdenado.innerHTML = "";
}

//Generación de Objetos

// Función que ordena el arreglo de la lista
function ordenar2(ordenador2){
    for(i=0;i<ordenador2.length;i++){
        ordenador2.sort((a,b) => a - b)
    }

    return ordenador2
}

 
//Función que genera la lista 
function genera(){
    let limite = document.getElementById('limite').value;
    let lstNumeros = document.getElementById('idNumeros');
    let arreglo = [];
   
    // Limpiar las opciones del select
    while(lstNumeros.options.length>0){
        lstNumeros.remove(0);
    }

         // Generar numeros aleatorios del 1 - 15

    for(let con=0;con<limite;con++){
        
        //lstNumeros.options[con] = new Option(con, 'texto', con)
        
        //lstNumeros.options[con] = new Option((Math.ceil(Math.random()*(50-1))));
        
        let aleatorio = Math.floor(Math.random()*50)+1;
        lstNumeros.options[con] = new Option(aleatorio,'valor:' + con);
        arreglo[con] = aleatorio;
            
                
    }

    let orden = ordenar2(arreglo);
    let contPares = 0
    let contImpares = 0

    for(let con=0;con<limite; con ++){
        lstNumeros.options[con] = new Option((orden[con]))
      
    }

    for(let con=0;con<limite;con ++){
        if(orden[con] % 2 == 0){
            contPares = contPares + 1;
        }else if(orden[con] % 2 != 0){
            contImpares = contImpares + 1; 
        }
    }

    

    //Calcular porcentaje de pares e impares

    console.log(limite)

    let porcentajePares = (contPares / limite) * 100;
    let porcentajeImpares = (contImpares / limite) * 100;

    let porPares = document.getElementById('porcentajePares');
    porPares.innerHTML = porcentajePares.toFixed(2)+"%";
    let porImpares = document.getElementById('porcentajeImpares')
    porImpares.innerHTML = porcentajeImpares.toFixed(2)+"%";

    let pSimetrico = document.getElementById('Psimetrico');

    if((porcentajePares - porcentajeImpares > 25) || (porcentajeImpares - porcentajePares > 25)){
        pSimetrico.innerHTML = "No es simetrico";
    }else if((porcentajePares - porcentajeImpares < 25) || (porcentajeImpares - porcentajePares < 25)){
        pSimetrico.innerHTML = "Sí es simetrico";
    }else{
        pSimetrico.innerHTML = "";
    }
    
    
    

}

// Función que valida la entrada de datos
function validar(){
    validarLimite = document.querySelector('#limite').value;
    limpiar = document.getElementById('limite');
    porPares = document.getElementById('porcentajePares');
    porImpares = document.getElementById('porcentajeImpares');
    simetrico = document.getElementById('Psimetrico');


    if(validarLimite == 0){
        alert('Debe capturar un valor para continuar');
        limpiar.value = "";
        porPares.innerHTML = "";
        porImpares.innerHTML = "";
        simetrico.innerHTML = "";
        
    }
}


// Función para limpiar todo
function borrar(){
        
        limpiar.value = "";
        porPares.innerHTML = "";
        porImpares.innerHTML = "";
        simetrico.innerHTML = "";
        
}





